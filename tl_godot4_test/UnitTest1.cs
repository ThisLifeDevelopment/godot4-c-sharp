using System.Drawing;
using System.Numerics;

namespace tl_godot4_test;

public class UnitTest1
{
    /// <summary>
    /// Tests that an object in a square is recognized as in the object.
    /// This one uses a simple, convex shape, as this is the most simple test.
    /// </summary>
    [Fact]
    public void AreasRecognizePointsInTheirBounds()
    {
        var point = new Godot.Vector3(0,0,0);
        var newArea = new area( new List<Godot.Vector2> () {
            new Godot.Vector2(100, 100),
            new Godot.Vector2(-100, 100),
            new Godot.Vector2(-100, -100),
            new Godot.Vector2(100, -100),
        });
        Assert.True(newArea.inArea(point));
    }
    // <summary>
    /// Tests that an object not in a square is recognized as not in the object.
    /// This one uses a simple, convex shape, as this is the most simple test.
    /// </summary>
    [Fact]
    public void AreasDoNotRecognizePointsOutOfTheirBounds()
    {
        var point = new Godot.Vector3(200,0,0);
        var newArea = new area( new List<Godot.Vector2> () {
            new Godot.Vector2(100, 100),
            new Godot.Vector2(-100, 100),
            new Godot.Vector2(-100, -100),
            new Godot.Vector2(100, -100),
        });
        Assert.False(newArea.inArea(point));
    }
}
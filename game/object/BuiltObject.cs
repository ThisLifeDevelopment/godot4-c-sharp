using Godot;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
[Tool]
public partial class BuiltObject : Node3D
{
	[Export]
	public string ObjId {get;set;}
	private MeshInstance3D? _meshChild;
	private Mesh _mesh = new BoxMesh();
	[Export]
	public Mesh Mesh {get {
		if (_meshChild != null)
		{
			return _meshChild.Mesh;
		}
		return  _mesh;
	}
	set {
		_mesh = value;
		if (_meshChild != null)
		{
			_meshChild.Mesh = value;
		}
	}
	}
	public HashSet<string> tags {get;set;}
	//different tasks may have different positions - for example, a chair may have a different position to sit in than it does to purchase.
	//This does not account for double beds.
	public area location {get;set;}
	public delegate void mouseEventHandler(BuiltObject obj);
	public delegate void clickEventHandler(BuiltObject obj,InputEventMouseButton buttonEvent);
	public static event mouseEventHandler HoverStart;
	public static event mouseEventHandler HoverStop;
	public static event clickEventHandler clicked;

	public BuiltObject() {
		ObjId = "NO ID";
		tags = new HashSet<string>();
	}
	public Vector3 GetCharacterTargetPosition(string task)
	{
		return this.Position + Vector3.Forward;
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		if (_meshChild == null)
		{
		_meshChild = new MeshInstance3D()
		{
			Mesh = _mesh
		};
		AddChild(_meshChild);
		}
		if (Engine.IsEditorHint())
		{
			return;
		}
		
		if (location == null)
		{
			location = GetParent<area>();
		}
		var child = GetNode<StaticBody3D>("body");
	
		child.MouseEntered += this.hoverStart;
		child.MouseExited += this.hoverStop;
		child.InputEvent += this.connectorTest;
	}

	protected void hoverStop()
	{
		HoverStop.Invoke(this);
	}

	protected void hoverStart()
	{
		HoverStart.Invoke(this);
	}

	protected void connectorTest(Node camera, InputEvent @event, Vector3 position, Vector3 normal, long shapeIdx)
	{
		if (@event is InputEventMouseButton buttonEvent)
		{
			clicked.Invoke(this,buttonEvent);
		}
	}


	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
}

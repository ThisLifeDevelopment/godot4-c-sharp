using Godot;
using System;
/// <summary>
/// The script that coordinates the whole thing.
/// Try to delegate as much as possible.
/// </summary>
public partial class GameScript : Node3D
{
	characterBase? currentCharacter = null;
	[Export]
	ActiveWorldInterface ui;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		BuiltObject.clicked += onObjClicked;
		BuiltObject.HoverStart += objHoverStart;
		BuiltObject.HoverStop += objHoverStop;
	}

	private void objHoverStop(BuiltObject obj)
	{
		ui.objHoverStop(obj);
	}

	private void objHoverStart(BuiltObject obj)
	{
		ui.objHoverStart(obj);
	}

	private void onObjClicked(BuiltObject obj, InputEventMouseButton buttonEvent)
	{
		GD.Print("GS Clicky");
		ui.onObjClicked(obj, buttonEvent);
		
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		
	}
}

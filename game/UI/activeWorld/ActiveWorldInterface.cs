using Godot;
using System;

public partial class ActiveWorldInterface : Control
{
	public BuiltObject? activeObj;
	public characterBase? activeChar;
	public BuiltObject? hoverObj;
	public double timeHovered;
	private Label? label;
	private RadialMenu? interactable;
	public void onObjClicked(BuiltObject obj, InputEventMouseButton buttonEvent)
	{
		GD.Print("Clicky", buttonEvent.Position);
		if (buttonEvent.IsPressed())
		{
			if (buttonEvent.ButtonIndex == MouseButton.Right)
			{
				if (obj is characterBase clickedChar)
				{
					//limit to owned characters
					activeChar = clickedChar;
				}
			}
			if (buttonEvent.ButtonIndex == MouseButton.Left)
			{
				if (activeChar == null)
					return;
				activeObj = obj;
				if (interactable != null)
				{
					interactable.QueueFree();
					interactable = null;
				}
				var RadialMenu = new RadialMenu() {
					Pos = buttonEvent.Position,
					options = new() {
						"Example Task Listing",
						"Hello World",
						"Second Example Listing",
						"option",
						"another",
						"sample",
						"show",
					},
				};
				RadialMenu.onMenuClosedEvent += menuClosed;
				RadialMenu.onOptionSelectedEvent += optionSelected;
				
				interactable = RadialMenu;
				AddChild(interactable);
			}
		}
	}
	private void optionSelected(int index)
	{
		GD.Print("SELECTED OPTION", index);
		var taskToDo = new Task(){ baseObject = activeObj!};
		taskToDo.steps.Enqueue(new Step(Step.Steps.goToObj));
		activeChar!.Tasks.Enqueue(taskToDo);
		menuClosed();
	}
	private void menuClosed()
	{
		interactable?.QueueFree();
		activeObj = null;
		interactable = null;
	}
	public void objHoverStart(BuiltObject obj)
	{
		hoverObj = obj;
		timeHovered = 0;
		GD.Print("UI hover start");
	}
	public void objHoverStop(BuiltObject obj)
	{
		if (hoverObj == obj)
		{
			hoverObj = null;
			if (label != null)
			{
				label.QueueFree();
				label = null;
			}
		}
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (hoverObj != null)
		{
			timeHovered += delta;
			if (label == null && timeHovered > 1)
			{
				label = new Label();
				label.SetPosition(GetLocalMousePosition() + new Vector2(0, 14));
				label.Text = hoverObj.ObjId;
				AddChild(label);
			}
		}
	}
}

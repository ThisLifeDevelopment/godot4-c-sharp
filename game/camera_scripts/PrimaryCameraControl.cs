using Godot;
using System;

public partial class PrimaryCameraControl : Camera3D
{
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}
	//Tilt forward or back
	public void shiftPitch(int dir, double delta) {

	}
	//tilt left or right
	public void shiftRoll(int dir, double delta) {

	}
	//turn left or right
	public void shiftYaw(int dir, double delta) {

	}
	//forward and back
	public void shiftForward(int dir, double delta) {
		//scroll wheel?
		
	}
	//left and right
	public void shiftSide(int dir, double delta) {

	}
	//up and down
	public void shiftUp(int dir, double delta) {

	}
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		
	}
}

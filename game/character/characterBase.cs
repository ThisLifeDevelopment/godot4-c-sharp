using Godot;
using System;
using System.Collections.Generic;

public partial class characterBase : BuiltObject
{
	[Export]
	public string characterName {get;set;}
	public Queue<Task> Tasks {get;set;}
	// Called when the node enters the scene tree for the first time.
	public characterBase() : base() 
	{
		Tasks = new Queue<Task>();
		tags.Add("character");
	}
	public override void _Ready()
	{
		Tasks = new Queue<Task>();
		var clickable = GetNode<CharacterBody3D>("CharacterBody3D");
		clickable.MouseEntered += hoverStart;
		clickable.MouseExited += hoverStop;
		clickable.InputEvent += connectorTest;
	}
	public void interactWithObject(BuiltObject target, string task) {
		var testTask = new Task() 
		{
			baseObject = target,
			taskName = task,
		};

		testTask.steps.Enqueue(new Step(Step.Steps.goToObj));
		testTask.steps.Enqueue(new Step(Step.Steps.findObj) {parameters = new() {{"searchTag", "stove"}},});
		testTask.steps.Enqueue(new Step(Step.Steps.goToObj));
		testTask.steps.Enqueue(new Step(Step.Steps.findObj) {parameters = new() {{"searchTag", "fridge"}},});
		testTask.steps.Enqueue(new Step(Step.Steps.goToObj));

		Tasks.Enqueue(testTask);
	}
	
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (Tasks.Count > 0)
		{
			Task current = Tasks.Peek();
			if (current.complete)
			{
				Tasks.Dequeue();
				return;
			}
			current.Act(this, (float)delta);
		}
	}
	public Vector3 getPosition(string task)
	{
		return this.Position;
	}
}

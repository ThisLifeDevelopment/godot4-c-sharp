using Godot;
using Godot.NativeInterop;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
[Tool]
public partial class terrain : Node3D
{
    private int _size = 8;
    [Export]
    public int size {
        get {
            return _size;
        }
        set
        {
            _size = value;
            if (mesh != null)
            {
                buildArray(value);
                buildMap();
                buildLines();
            }
            
        }
    }

	private List<Vector3> arrays;
    private MeshInstance3D? mesh;
    private MeshInstance3D? lines;
    private int getPos(int x, int y)
    {
        var row = x*(_size+1);
        var col = y;
        return row + col;
    }

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
        buildArray(_size);
        buildMap();
        buildLines();
	}
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
    private void buildLines()
    {
        if (lines != null)
            lines.Free();

        var arraymesh = new ArrayMesh();

        var gdArray = new Godot.Collections.Array();
        gdArray.Resize((int)Mesh.ArrayType.Max);
        
        for (int i = 0; i < size+1; i++)
        {
            gdArray[(int)Mesh.ArrayType.Vertex] = buildLines(false, i);
            arraymesh.AddSurfaceFromArrays(Mesh.PrimitiveType.LineStrip, gdArray);
            gdArray[(int)Mesh.ArrayType.Vertex] = buildLines(true, i);
            arraymesh.AddSurfaceFromArrays(Mesh.PrimitiveType.LineStrip, gdArray);
        }
        lines = new MeshInstance3D();
        lines.Mesh = arraymesh;
        AddChild(lines);
    }
    private Vector3[] buildLines(bool buildingX, int pos)
    {
        var lineStrip = new List<Vector3>();
        for (var i = 0; i <= size; i++)
        {
            if (buildingX)
            {
                lineStrip.Add(arrays[getPos(pos, i)] + (Vector3.Up * 0.1f));
            }
            else
            {
                lineStrip.Add(arrays[getPos(i, pos)] + (Vector3.Up * 0.1f));
            }
        }
        return lineStrip.ToArray();
    }
    private void buildMap()
    {
        if (mesh != null)
            mesh.Free();

        var arraymesh = new ArrayMesh();

        var gdArray = new Godot.Collections.Array();
        gdArray.Resize((int)Mesh.ArrayType.Max);
        
        for (int i = 0; i < size; i++)
        {
            (gdArray[(int)Mesh.ArrayType.Vertex], gdArray[(int)Mesh.ArrayType.Color]) = toTriangles(i);
            arraymesh.AddSurfaceFromArrays(Mesh.PrimitiveType.TriangleStrip, gdArray);
        }
        mesh = new MeshInstance3D();
        mesh.Mesh = arraymesh;
        AddChild(mesh);
    }
    private (Vector3[], Godot.Color[]) toTriangles(int posy)
    {
        var points = new List<Vector3>();
        var color = new List<Godot.Color>();
        //start with the first two
        
        for (int posx = 0; posx <= size; posx++)
        {
            points.Add(arrays[getPos(posx,posy+1)]);
            points.Add(arrays[getPos(posx,posy)]);
            color.Add(Colors.Green);
            color.Add(Colors.Green);
        }
        
        return (points.ToArray(), color.ToArray());
    }

    private void buildArray(int newSize) 
    {
        arrays = Enumerable.Repeat(new Vector3(), (newSize+1)*(newSize+1)).ToList();
        for (var posx = 0; posx <= newSize; posx++)
        {
            for (var posy = 0; posy <= newSize; posy++)
            {
                arrays[getPos(posx, posy)] = new Vector3(posx, 0, posy);
            }
        }
    }



}

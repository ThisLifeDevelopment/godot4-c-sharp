using Godot;
using Godot.NativeInterop;
using System;
using System.Collections.Generic;

public partial class area : Node3D
{
	public List<Vector2> areaDeffinition;
	public area()
	{
		areaDeffinition = new List<Vector2>() {
			new(-100, -100),
			new(100, -100),
			new(100,100),
			new(100, -100)
		};
	}
	public area(List<Vector2> space)
	{
		areaDeffinition = space;
	}
	public List<BuiltObject> getObjectsOfType(string type) 
	{
		var foundObjects = new List<BuiltObject>();
		var children = GetChildren();
		foreach (var child in children)
		{
			if (child is BuiltObject foundObj)
			{
				if (foundObj.tags.Contains(type))
				{
					foundObjects.Add(foundObj);
				}
			}
		}
		return foundObjects;
	}

	public bool inArea(Vector3 position)
	{
		if (areaDeffinition.Count == 0)
		{
			return false;
		}
		Geometry2D.IsPointInPolygon( new Vector2(position.X, position.Z), areaDeffinition.ToArray());
		return false;
	}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{

	}
	public void addObject(BuiltObject obj)
	{
		AddChild(obj);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
}

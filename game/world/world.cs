using Godot;
using System;

public partial class world : Node3D
{
	public delegate void objInteractEventHandler(BuiltObject obj);
	public delegate void objClickEventHandler(BuiltObject obj, InputEventMouseButton buttonEvent);
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		var newObject = GD.Load<PackedScene>("res://object/Object.tscn");
		var stove = newObject.Instantiate<BuiltObject>();
		stove.Position = new Vector3(15, 0, 10);
		
		stove.ObjId = "Stove";
		var meshFile = GD.Load<ArrayMesh>("res://StuffAndAssets/kennyAssets/Models/kitchenStoveElectric.obj");
		stove.Mesh = meshFile;
		stove.tags.Add( "kitchenEquipment" );
		stove.tags.Add("stove");
		var area = GetNode("Plot").GetNode<area>("Area");
		area.addObject(stove);

		characterBase character = GetNode<characterBase>("Plot/character");
		BuiltObject fridge = GetNode<BuiltObject>("Plot/Area/Fridge");
		fridge.tags.Add("fridge");
		character.interactWithObject(fridge, "getFood");
	}
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
}

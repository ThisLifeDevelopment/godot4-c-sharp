using System;
using System.Collections.Generic;
using Godot;

public class Task
{

	public BuiltObject baseObject {get;set;}
	public string taskName {get;set;}
	public bool complete = false;
	public Queue<Step> steps {get;set;}
	public Task() {
		steps = new Queue<Step>();
	}
	public void Act(characterBase actor, float delta) {
		if (steps.Count == 0)
		{
			complete = true;
		}
		if (complete)
			return;
		var step = steps.Peek();
		
		switch (step.task)
		{
			case Step.Steps.findObj:
				findObject(actor, step.parameters, delta);
				break;
			case Step.Steps.goToObj:
				goToObject(actor, step.parameters, delta);
				break;
			default: //step is missing, mark it as done so we don't get studk.
				GD.Print("NO STEP FOUND",step.task);
				step.complete = true;
				break;
		}
	}
	public void findObject(characterBase actor, Dictionary<string, string> parameters, float delta) {
		string searchTag = parameters["searchTag"];
		GD.Print("Finding object of type ", searchTag);
        BuiltObject obj = baseObject;
        var toSearch = obj.location;
        var results = toSearch.getObjectsOfType(searchTag);
        baseObject = results[0];
        steps.Dequeue();
	}
	public void goToObject(characterBase actor, Dictionary<string, string> parameters, float delta)
	{
		var stepTag = "";
		if (parameters.ContainsKey("stepTag"))
		{
			stepTag = parameters["stepTag"];
		}
		var targetPos = baseObject.GetCharacterTargetPosition(stepTag);
		actor.LookAt(targetPos);
		actor.Position = actor.Position.MoveToward(targetPos, delta);
        if (actor.Position == targetPos)
			steps.Dequeue();
	}

}

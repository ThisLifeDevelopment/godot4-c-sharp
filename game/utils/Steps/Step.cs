using System.Collections.Generic;

public class Step {
    public Step(Steps newStep)
    {
        task = newStep;
    }
    public enum Steps 
    {
        findObj,
        goToObj,
    }
    public Steps task {get;set;} = Steps.goToObj;
    public Dictionary<string, string> parameters {get;set;} = new Dictionary<string, string>();
    public bool complete {get;set;} = false;
}
using Godot;

public partial class RadialButton : Button
{
    private Vector2 _radialPos = Vector2.Up;
    [Export]
    public Vector2 radialPosition {get {return _radialPos;} 
    set {
        _radialPos = value;
        positionSelf();
    }}
    private Vector2 _radialAngle = Vector2.Zero;
    [Export]
    public Vector2 radialAngle {get {return _radialAngle;}
    set {
        _radialAngle = value;
        positionSelf();
    }}
    public string OptionText {
        get {
            return this.Text;
        }
        set {
            this.Text = value;
        }}

    public override void _Ready()
    {
        positionSelf();
    }
    private void positionSelf()
    {
        var magicNum =0.001; //how far to the left or right before it switches anchors. set so that 0 will not have any side anchor
		var pos = radialPosition;
        var rect = GetRect();
        
		if (this.radialAngle.X > magicNum) 
		{
		}
		else if (this.radialAngle.X < -magicNum)
		{
            pos.X -= rect.Size.X;
		}
        else
        {
            pos.X -= rect.Size.X/2;
        }
        
        SetPosition(pos);
    }
}
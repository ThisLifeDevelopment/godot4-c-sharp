using Godot;
using System;
using System.Collections.Generic;

public partial class RadialMenu : Control
{
	private List<string> _options = new List<string>();
	public List<string> options {
		get {
		return _options;
	} 
	set {
		_options = value;
		buildDisplay();
	}} 
	public delegate void optionSelected(int selected);
	public event optionSelected? onOptionSelectedEvent;
	public delegate void menuClosed();
	public event menuClosed? onMenuClosedEvent;
	public Button? closeButton;
	private Vector2 pos;
	public Vector2 Pos {get {
		return Position;
	}
	set {
		Position = value;
	}}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		buildDisplay();
	}
	private void buildDisplay() {
		foreach (var child in this.GetChildren())
		{
			child.QueueFree();
		}
		addCloseButton();
		for (var i = 0; i < options.Count; i++)
		{
			addButton(i);
		}
	}
	private void addCloseButton()
	{
		closeButton = new Button();
		closeButton.MouseFilter = MouseFilterEnum.Stop;
		closeButton.Text = "X";
		closeButton.Pressed += () => onMenuClosedEvent?.Invoke();
		closeButton.SetAnchorsPreset(LayoutPreset.Center);
		AddChild(closeButton);
	}
	private void addButton(int option)
	{
		var distance = 70;
		var angle = Vector2.FromAngle(getAngleOfButton(option, options.Count));
		var radialButton = new RadialButton() 
		{
			OptionText = options[option],
			radialAngle = angle,
			radialPosition = angle * distance + pos,
		};
		
		radialButton.MouseFilter = MouseFilterEnum.Stop;
		radialButton.Pressed += () => onOptionSelectedEvent?.Invoke(option);

		AddChild(radialButton);
	}

	private float getAngleOfButton(int position, int maxPosition)
	{
		var radsInCircle = 2* ((float)Math.PI);
		var optionAngle = radsInCircle * position / maxPosition;
		var fixRotation = optionAngle - radsInCircle/4;
		return (float)fixRotation;
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
}
